import React, { useState, useEffect } from 'react'
import { Grid, Card, CardContent, TextField, Button, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux';
import FormError from '../usableComponent/FormError';
import { makeStyles } from '@mui/styles'
import { useNavigate } from 'react-router-dom';
import { adminLogin } from '../../redux/action';

const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }
}));


const Login = () => {

  const classes = useStyles();
  const [data, setData] = useState({
    password: '',
    email: '',
  })

  const [err, setErr] = useState({})
  const dispatch = useDispatch()
  const userAuth = useSelector(state => state.auth.isAuthenticated)
  const loading = useSelector(state => state.auth.loader)
  const history = useNavigate()

  useEffect(() => {

    if (userAuth) {
      history('/dashboard')
    }

  }, [userAuth])


  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    setErr({ ...err, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    var check = loginValidate(data)
    setErr(check)

    if (Object.keys(check).length > 0) return
    console.log('formData', data)
    dispatch(adminLogin(data))
  }

  const handleReset = () => {
    setData({
      ...data,
      email: '',
      password: ''
    })
  }

  return (
    <Grid style={{ padding: '150px 0' }}>
      <div className={classes.paper}>
        <Card>
          <CardContent>
            <form>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Typography flexGrow={1} variant="h4" component="h2" align="center">
                    Login
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="email" type="text" value={data.email} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Email" />
                  {err.email && (<FormError data={err.email}></FormError>)}
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                  <TextField name="password" type="password" value={data.password} fullWidth variant="outlined" InputLabelProps={{ shrink: true }} onChange={handleChange} label="Password" />
                  {err.password && (<FormError data={err.password}></FormError>)}
                </Grid>
              </Grid>
              <Grid container spacing={2} sx={{ marginTop: '5px' }}>
                <Grid item xs={6} align="left">
                  <Button type="button" disabled={loading ? true : false} variant="contained" onClick={handleReset} color="primary">Reset</Button>
                </Grid>
                <Grid item xs={6} align="right">
                  <Button type="button" disabled={(loading) ? true : false} variant="contained" onClick={() => { handleSubmit() }} color="primary">Login</Button>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </div>
    </Grid>
  )
}

export default Login

const loginValidate = (data) => {
  let errors = {}

  if (!data.email) {
    errors["email"] = "Email is Required"
  }

  if (data.email) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!pattern.test(data["email"])) {
      errors["email"] = "Please enter valid email.";
    }
  }

  if (!data.password) {
    errors["password"] = "Password is Required"
  }

  return errors
}