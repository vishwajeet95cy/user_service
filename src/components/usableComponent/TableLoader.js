import React from 'react';
import { Skeleton } from '@mui/material';

const TableLoader = () => {
  return (
    <div>
      <Skeleton />
      <Skeleton animation={false} />
      <Skeleton animation="wave" />
    </div>
  );
}

export default TableLoader
