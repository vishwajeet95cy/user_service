import { makeStyles } from '@mui/styles'

export const tableStyles = makeStyles(() => ({
  grid: {
    paddingInline: '15px',
    paddingBlock: '15px',
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
  mobilegrid: {
    backgroundColor: '#342f2f !important'
  },
  cardTitle: {
    fontSize: '0.75rem'
  },
  cardValue: {
    fontSize: '1.0rem'
  },
  tabgrid: {
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
  ssgrid: {
    paddingInline: '15px',
    paddingBlock: '15px',
    justifyContent: 'center',
  },
}))

export const statusStyles = makeStyles(() => ({
  waiting: {
    fontSize: '1.2rem !important',
    color: 'red !important',
    backgroundColor: 'null !important'
  },
  accepted: {
    fontSize: '1.2rem !important',
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  running: {
    fontSize: '1.2rem !important',
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  cancelled: {
    fontSize: '1.2rem !important',
    color: 'red !important',
    backgroundColor: 'null !important'
  },
  completed: {
    fontSize: '1.2rem !important',
    color: '#19b819 !important',
    backgroundColor: 'null !important'
  },
  expired: {
    fontSize: '1.2rem !important',
    color: 'red !important',
    backgroundColor: 'null !important'
  }
}))

export const CircularTimer = makeStyles(() => ({
  timer: {
    position: 'absolute',
    top: '94px',
    left: '44%'
  }
}))

export const DetailStyles = makeStyles(() => ({
  grid: {
    paddingInline: '15px',
    paddingBlock: '15px',
    justifyContent: 'center',
    overflowX: 'auto',
  }
}))

export const employeeStyles = makeStyles(() => ({
  grid: {
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
  mobilegrid: {
    backgroundColor: '#342f2f !important'
  },
  cardgrid: {
    paddingInline: '15px',
    justifyContent: 'center',
    overflowX: 'auto',
    height: '100%'
  },
}))

export const tabStyles = makeStyles(() => ({
  font: {
    fontSize: '10px'
  },
  grid: {
    color: '#C31818',
    cursor: 'pointer'
  },
  selectGrid: {
    color: '#0a58ca',
    cursor: 'pointer'
  }
}))

export const mobileviewStyles = makeStyles(() => ({
  deleteColor: {
    color: 'red'
  },
  mobileCardTitle: {
    fontSize: '1rem'
  },
  mobileCardData: {
    fontSize: '1.125rem'
  },
  table: {
    '&:last-child td, &:last-child th': { border: 0 }
  },
  mobileCardBtn: {
    textTransform: 'capitalize',
    fontSize: '0.75rem'
  },
  cardTitleM: {
    fontSize: '0.75rem'
  },
  cardBodyM: {
    fontSize: '0.95rem'
  },
  cardContentM: {
    paddingBottom: '0 !important'
  },
  mobileTable: {
    display: 'flex', justifyContent: 'center', flexDirection: 'column'
  },
  cardMediaM: {
    width: '100%',
    objectFit: 'unset',
    borderRadius: '18px',
    border: 1
  },
}));